---
title: "About"
date: 2020-12-06T01:44:54-05:00
draft: false
---

# About me

I am an e-commerce web developer based in Montreal, Quebec.

I work on both front-end and back-end integrations on the Salesforce Commerce Cloud platform. 

## Technologies 

- JavaScript
- HTML/CSS
- Grunt/Webpack



